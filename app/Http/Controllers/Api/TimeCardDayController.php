<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TimeCardDayController extends Controller
{
    var $newHoursStr;
    var $refer;
    function TimeCardDayController($refer){
        $this->refer = $refer;
    }
    public function order(Request $request){
        try{
            try{
                $this->validate($request, ['*.timecarddays.*.date'=>'required|date|date_format:Y-m-d']);//
            }catch(ValidationException $e){
                return response()->json($e->errors());
            }
            $arraySaida=[];
            $i=0;
            foreach($request->request as $employeeInfo){
                $arraySaida[$i]['employee'] = substr($employeeInfo['employee'], -12, 12);
                $this->refer = $employeeInfo['refer'];

                foreach($employeeInfo["timecarddays"] as $days){

                    $hoursStr = [];
                    foreach($days['hours'] as $hours){
                        $hoursStr[] = substr_replace($hours, '', strpos($hours, '-'));
                    }

                    $arrayHoursOrdered=[];$indicesAchados=[];
                    foreach($hoursStr as $hora){
                        //Verifica o indice do Array $refer que esta mais perto, pela media dos valores
                        //da hora atual e constroi um novo array
                        $indiceAlvo = $this->getKeyTarget(intval(Carbon::parse($hora)->format('H')), $this->refer, $indicesAchados);
                        $arrayHoursOrdered[$indiceAlvo] = $hora;
                        $indicesAchados[]=$indiceAlvo;
                    }

                    ksort($arrayHoursOrdered);
                    $arraySaida[$i][$days['date']] = implode('|', $arrayHoursOrdered);
                }
                $i++;
            }
            return response()->json($arraySaida);
        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }

    function getKeyTarget($hora, $arr, $indicesAchados) {
        $horaAprox = null;
        foreach ($arr as $key => $itemRefer)
        {
            $itemReferOrig = $itemRefer;
            $itemRefer = intval(Carbon::parse($itemRefer)->format('H'));
            if ($horaAprox === null || ( abs($hora - $horaAprox) > abs($itemRefer - $hora) ) ) {
                $horaAprox = $itemRefer;
                $result = $itemReferOrig;
            }
        }
        $keyAchado = array_search($result, $arr);
        if(in_array($keyAchado, $indicesAchados)){
            if(!in_array($keyAchado+1, $indicesAchados))
                return $keyAchado+1;
            elseif(!in_array($keyAchado-1, $indicesAchados))
                return $keyAchado-1;
        }else{
            return $keyAchado;
        }
    }
}
